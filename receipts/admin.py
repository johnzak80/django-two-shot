from django.contrib import admin
from .models import Receipt, Account, ExpenseCategory
# Register your models here.



@admin.register(Receipt)
class RecepitAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        
    )

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
       "name",
    )

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
       "name",
    )