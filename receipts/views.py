from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def receipt_list(request):
     # This line creates a queryset that filters Receipt objects
    # to only include those where the purchaser is the logged-in user.
    receipts = Receipt.objects.filter(purchaser=request.user)
    context ={
        'receipts': receipts
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)  # Create but don't save the object yet
            receipt.purchaser = request.user
            receipt.save()  # Save the object with the updated purchaser field
            return redirect('home')  # Redirect to the list of receipts
    else:
        form = ReceiptForm()
    context = {
        'form' : form
    }

    return render(request, 'receipts/create.html', context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner = request.user)
    context ={
        'categories' : categories,
    }

    return render(request, 'categories/list.html', context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner = request.user)
    context ={
        'accounts' : accounts
    }

    return render(request, 'accounts/list.html', context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            cat = form.save(commit=False)  # Create but don't save the object yet
            cat.owner = request.user
            cat.save()  # Save the object with the updated purchaser field
            return redirect('category_list')  # Redirect to the list of receipts
    else:
        form = ExpenseCategoryForm()
    context = {
        'form' : form
    }

    return render(request, 'categories/create.html', context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            acc = form.save(commit=False)  # Create but don't save the object yet
            acc.owner = request.user
            acc.save()  # Save the object with the updated purchaser field
            return redirect('account_list')  # Redirect to the list of receipts
    else:
        form = AccountForm()
    context = {
        'form' : form
    }

    return render(request, 'accounts/create.html', context)