# One Shot Planning

These are some markdown formatting you can use to plan. You can preview your markdown in VS Code with the command "markdown preview", commonly `Command + Shift + V` (`Ctrl + Shift + V` on Windows) while you have this file open

Edit this and make it your own. Alternatively, link your notion here:

## My Notion / weblink

https://notion...

## Steps

### Step 1

* [ ] python -m venv ./venv
* [ ] source venv/Scripts/activate
* [ ] pip install django
* [ ] python.exe -m pip install --upgrade pip
* [ ] pip install black
* [ ] pip install flake8
* [ ] pip install djlint
* [ ] python -m pip install django-debug-toolbar
* [ ] pip freeze > requirement.txt
* [ ] push to repo

### Step 2

* [ ] django-admin startproject 
* [ ]  python manage.py startapp 
* [ ] python manage.py makemigrations 
* [ ] python manage.py migrate
* [ ] python manage.py createsuperuser
* [ ] python manage.py runserver
* [ ] http://localhost:8000/admin/
### Step 3

To utilize user in foreign key:
from django.conf import settings
<namehere> = models.ForeignKey(
        settings.AUTH_USER_MODEL,)
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 

## Questions

How do I access the model instances of the reverse relationship of a many to many relationship?

## Resources

* https://ccbv.co.uk/
